import argparse

from . import example


def minimal_example():
    """Entry point for the minimal example"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--radius",
        required=True,
        type=float,
        help="Radius of circle.",
    )
    parser.add_argument(
        "--print-data",
        required=False,
        action="store_true",
        help="Print info from data file.",
    )

    args = parser.parse_args()

    example.print_something(args.radius)
    if args.print_data:
        example.print_some_data()
